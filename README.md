# aktoro

An actor library written in Rust

This library is limited to Actors as a unit of processing

Actors have the following properties:

1) An actor's state is immutable
2) An actor exposes a single public method (what about internal lifecycle methods?)
3) The public method takes a CID that references a CBOR-LD value (representing a graph dataset)
4) An actor may publicly expose the CIDs of zero or more of the shapes that it will pattern match as CBOR-LD
5) An actor may take the following actions (concurrently) when the method is called
    a) Create zero or more new actors
    b) Send zero or more messages (interesting how is this possible if messages are not there yet)
    c) Set a new behaviour//state for the next message (function call)
    d) Terminate itself



