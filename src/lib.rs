
pub fn public_function() {
    println!("called rary's `public_function()`");
}

fn private_function() {
    println!("called rary's `private_function()`");
}

pub fn indirect_access() {
    print!("called rary's `indirect_access()`, that\n> ");

    private_function();
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn public_function_test() {
        public_function();
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn indirect_access_test() {
        indirect_access();
        assert_eq!(2 + 2, 4);
    }
}

